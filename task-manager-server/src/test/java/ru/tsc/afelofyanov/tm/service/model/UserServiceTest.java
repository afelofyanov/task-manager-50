package ru.tsc.afelofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.User;

public class UserServiceTest extends AbstractTest {

    @Before
    @Override
    public void init() {
        super.init();
    }

    @After
    @Override
    public void close() {
        super.close();
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("test", null));

        @Nullable User user = userService.create("testCreate", "testCreate");
        Assert.assertNotNull(user);
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.createWithEmail(null, null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.createWithEmail("test", null, null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.createWithEmail("test", "test", null));
        Assert.assertThrows(LoginExistException.class, () -> userService.createWithEmail("testUser", "test", "test"));

        @Nullable User user = userService.createWithEmail("testCreateEmail", "testCreateEmail", "testCreateEmail");
        Assert.assertThrows(EmailExistException.class, () -> userService.createWithEmail("asfff", "test", "testCreateEmail"));
        Assert.assertNotNull(user);
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.createWithRole(null, null, null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.createWithRole("test", null, null, null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.createWithRole("test", "test", null, Role.USUAL));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.createWithRole("test", "test", "test", null));

        @Nullable User user = userService.createWithRole("testCreateRole", "testCreateRole", "testCreateRole", Role.USUAL);
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));

        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNull(userService.findByLogin("qwerty"));
        Assert.assertNotNull(userService.findByLogin("test"));
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));

        userService.createWithEmail("testFindEmail", "test", "testFindEmail");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNull(userService.findByEmail("qwerty"));
        Assert.assertNotNull(userService.findByEmail("testFindEmail"));
    }

    @Test
    public void remove() {
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(null));

        @NotNull User user = userService.create("removeTest", "test");
        @Nullable User userTest = userService.findByEmail("qwerty");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(userTest));
        Assert.assertNotNull(userService.remove(user));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));

        @NotNull User user = userService.create("removeLogin", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        userService.removeByLogin(user.getLogin());
        Assert.assertNull(userService.findByLogin("removeLogin"));
    }

    @Test
    public void isLoginExists() {
        @NotNull User user = userService.create("loginExist", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists("22222"));
        Assert.assertTrue(userService.isLoginExists(user.getLogin()));
    }

    @Test
    public void isEmailExists() {
        @NotNull User user = userService.createWithEmail("emailExist", "test", "emailExist");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(userService.isEmailExists(""));
        Assert.assertFalse(userService.isEmailExists("22222"));
        Assert.assertTrue(userService.isEmailExists(user.getEmail()));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword("", null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", ""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword("123", "234"));

        @NotNull User user = userService.create("setPass", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNotNull(userService.setPassword(user.getId(), "123"));
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.updateUser(null, "test", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", null, "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", null, "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "test", null));

        Assert.assertThrows(UserIdEmptyException.class, () -> userService.updateUser("", "test", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "test", ""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser("123", "test", "test", "test"));

        @NotNull User user = userService.create("updateUser", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNotNull(userService.updateUser(user.getId(), "123", "asd", "qwerty"));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("123"));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(user.getLocked());

        userService.lockUserByLogin("test");
        @Nullable User userLock = userService.findByLogin("test");
        Assert.assertNotNull(userLock);
        Assert.assertTrue(userLock.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("123"));

        @Nullable User user = userService.findByLogin("test");
        Assert.assertNotNull(user);

        userService.lockUserByLogin("test");
        @Nullable User userLock = userService.findByLogin("test");
        Assert.assertNotNull(userLock);
        Assert.assertTrue(userLock.getLocked());

        userService.unlockUserByLogin("test");
        @Nullable User userUnlock = userService.findByLogin("test");
        Assert.assertNotNull(userUnlock);
        Assert.assertFalse(userUnlock.getLocked());
    }
}
