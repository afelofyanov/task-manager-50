package ru.tsc.felofyanov.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.OperationEvent;
import ru.tsc.felofyanov.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import java.util.function.Consumer;

import static ru.tsc.felofyanov.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class EntityListener {

    private static Consumer<OperationEvent> CONSUMER = null;

    public static Consumer<OperationEvent> getConsumer() {
        return CONSUMER;
    }

    public static void setConsumer(Consumer<OperationEvent> consumer) {
        EntityListener.CONSUMER = consumer;
    }

    @PostLoad
    private void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PrePersist
    private void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    private void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PreRemove
    private void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    private void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PreUpdate
    private void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    private void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        if (CONSUMER == null) return;
        CONSUMER.accept(new OperationEvent(operationType, entity));
    }
}
