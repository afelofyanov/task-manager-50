package ru.tsc.felofyanov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.IReceiverService;

import javax.jms.*;

public class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull final ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue("TM_LOG");
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }
}
