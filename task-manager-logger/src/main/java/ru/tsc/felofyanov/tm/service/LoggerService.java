package ru.tsc.felofyanov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient client = new MongoClient("localhost", 27017);

    @NotNull
    private final MongoDatabase database = client.getDatabase("tm_log");

    @Override
    @SneakyThrows
    public void writeLog(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @Nullable final String tableName = event.get("table").toString();
        if (tableName == null || tableName.isEmpty()) return;
        if (database.getCollection(tableName) == null) database.createCollection(tableName);
        @NotNull final MongoCollection<Document> collection = database.getCollection(tableName);
        collection.insertOne(new Document(event));
    }
}

