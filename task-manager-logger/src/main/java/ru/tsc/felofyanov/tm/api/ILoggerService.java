package ru.tsc.felofyanov.tm.api;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void writeLog(@Nullable String message);
}
